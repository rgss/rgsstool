# RGSS Tool

Python 3 command-line tool to create and extract files from RPG Maker archives, an RPG Maker Decrypter/Extracter.

Currently it supports RPG Maker XP, VX, and VX Ace archives (RGSS versions 1, 2, 3).

To extract (or decrypt) files from an Archvie (`file.rgss3a`) to the current directory, use:

    rgsstool.py -x file.rgss3a

To create an Archive (`file.rgss3a`, RPG Maker VX Ace by default) from the contents of a directory (`Data/`), use:

    rgsstool.py -c -d Data file.rgss3a

or an RPG Maker XP Archive from the contents of two directories (`Data/` and `Graphics/`):

    rgsstool.py -c -1 file.rgssad Data/ Graphics/

Use the `--help` option for explanations of more parameters.